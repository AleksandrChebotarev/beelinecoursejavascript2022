
// Задание 1
const greetPerson = () => {
  const sayName = (name) => {
    return `hi, i am ${name}`
  }
  return sayName
}

const greeting = greetPerson(); // Присвоение переменной greeting ссылки на функцию greetPerson, внутри которой замкнута функция sayName. greetPerson возвращает значение функции sayName
console.log(greeting('Pavel')); // Вызывается функция greeting. Благодаря замыканию в sayName передается значение из агрумента ('Pavel') - это name и далее, 
                                // благодаря интерполяции строк в консоли получаем: hi, i am Pavel
console.log(greeting('Irina')); // Вызывается функция greeting. Благодаря замыканию в sayName передается значение из агрумента ('Irina') - это name и далее, 
                                // благодаря интерполяции строк в консоли получаем: hi, i am Irina



// Задание 2
let y = 'test';
const foo = () => {
  // var newItem = 'hello';
  console.log(y);
}
foo();                 // Вызываем функцию foo в результате которой выполняется console.log, который в аргумент принимает переменную y-она является глобальной
                       // и так как переменная объявлена выше функции через let Интерпретатор не найдя в локальной области видимости поднимится в глобальную область
                       // результат консоли: test
console.log(newItem);  // Так как переменная закоммичена, то ее не существует получаем ошибку, но если расскомитить результат останется тот же,
                       // так как переменная обхявлена в локальной области видимости функции foo.  Результат: Ошибка ReferenceError

// Задание 3
let y = 'test';
let test = 2;
const foo = () => {
  const test = 5;
  const bar = () => {
    console.log(5 + test);
  }
  bar()
}
foo();  // Вызывам функцию  foo, в которой создается локальная константа test со значением 5, и объявляется и вызывается функция bar, в результате которой будет найдена переменная test,
        // находит ее в локальной видимости равную 5 (интерпретатор ищет сначала в локальной облати видимости и останавливается в случае нахождения, но если нет - то поднимается в глобальную область видимости),
        // выполняется сложение и результат консоли: 10.

// Задание 5
const bar = () => {
  const b = 'no test'
}
bar();     // функция ничего не возвращает, объявлена локальная переменная, которая не доступна извне

const foo = (() => {
  console.log(b);
})();                // так как функция обёрнута в круглые скопки и вызвана в момент объявления (self-invoking functions), она ограничивается местом её вызова и не захвативает переменные извне,
                     // а так же в нее мы ничего не передаем на данный момент.
const b = 'test';    // Поэтому компилятор не найдя переменную b в локальной области видимости не будет искать в глобальной области видимости,
                     // следовательно в консоли увидим ошибку ReferenceError: Cannot access 'b' before initialization