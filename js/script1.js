// 1. Разобраться, что выводит код и написать объяснение в комментарий

let myVar;
myVar = 5;
let myNum;
myNum = myVar;  //myNum = 5
myVar = 10;		
myNum = myVar;	//myNum = 10

// 2. Разобраться, что выводит код и написать объяснение в комментарий

const myVar;
myVar = 5;
const myNum;
myNum = myVar;
myVar = 10;
myNum = myVar;

// Выводит: SyntaxError: Missing initializer in const declaration

// 3. Разобраться, что выводит код и написать объяснение в комментарий

let a = 123
let b = -123;
let c = "Hello";
const e = 123;

a = a + 1;            // 124
b =+ 5;				  // 5
c = c + " world!"	  // "Hello word"
e = e + 123;		  // TypeError: Assignment to constant variable

// 4. Разобраться, что выводит код и написать объяснение в комментарий

let a = 0;
let b;

if (a) {
	console.log(a);
} else if (b) {
	console.log(b);
} else {
	console.log('hello world');
}

// Выводит: 'hello world'

/** Обьяснить в комментарии к коду, что произошло */


// 5. Необходимо проверить на тип переменную a. Посмотреть, что выводит код и написать объяснение в комментарий

let a = "Hello world";

if (typeof a === "string") {
	console.log(a);
}

// Выводит: "Hello world".  Оператор typeof - возвращает тип данных в строковом формате,
// в данном случае переменная a - это строка, а дальше производится глубокое сравнение в условии оператора if, "string" === "string" это истина - выполнится код внутри.

// 6. Разобраться, что выводит код и написать объяснение в комментарий

let a = {};
let b = a;

alert( a == b );  	//true
alert( a === b );	//true

// Так как `a` - это объект, ссылочный тип данных, `b` присваеваем не сам объект `a`, а ссылку на него, следовательно ссылки будут одинаковые. оба alert - true